package agibank.reportmanagement.domain;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class Client {
    private String cnpj;
    private String name;
    private String businessArea;

    public Client(String cnpj, String name, String businessArea) {
        this.cnpj = cnpj;
        this.name = name;
        this.businessArea = businessArea;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(final String cnpj) {
        this.cnpj = cnpj;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(final String businessArea) {
        this.businessArea = businessArea;
    }
}
