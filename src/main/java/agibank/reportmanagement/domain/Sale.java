package agibank.reportmanagement.domain;

import java.util.List;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class Sale {
    private String id;
    private List<SaleItem> items;
    private String salesmanName;

    public Sale(final String id, final List<SaleItem> items, final String salesmanName) {
        this.id = id;
        this.items = items;
        this.salesmanName = salesmanName;
    }

    public double calculateTotalPrice(){
        return items
            .stream()
            .map(SaleItem::calculateTotalPrice)
            .reduce((s1, s2) -> s1 + s2)
            .get();
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public List<SaleItem> getItems() {
        return items;
    }

    public void setItems(final List<SaleItem> items) {
        this.items = items;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(final String salesmanName) {
        this.salesmanName = salesmanName;
    }
}
