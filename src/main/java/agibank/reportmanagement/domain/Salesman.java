package agibank.reportmanagement.domain;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class Salesman {
    private String cpf;
    private double salary;
    private String name;

    public Salesman(final String cpf, final String name, final double salary) {
        this.cpf = cpf;
        this.salary = salary;
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(final String cpf) {
        this.cpf = cpf;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(final double salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
