package agibank.reportmanagement.domain;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class SaleItem {
    private String id;
    private int quantity;
    private double itemPrice;

    public SaleItem(final String id, final int quantity, final double itemPrice) {
        this.id = id;
        this.quantity = quantity;
        this.itemPrice = itemPrice;
    }

    public double calculateTotalPrice(){
        return quantity * itemPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(final double itemPrice) {
        this.itemPrice = itemPrice;
    }
}
