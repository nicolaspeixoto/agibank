package agibank.reportmanagement.report;

import agibank.reportmanagement.Constants;
import agibank.reportmanagement.accumulator.Accumulator;

/**
 * Created by nsanto1 on 04/05/18.
 */
public interface Report {
    public void processLine(final String line);

    public String getOutput();
}
