package agibank.reportmanagement.report;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class ReportFactory {
    public static Report getReport(final String type){
        if(type == "sales") {
            return new SalesReport();
        }
        throw new ReportNotFoundException("Report " + type + " not found");
    }
}
