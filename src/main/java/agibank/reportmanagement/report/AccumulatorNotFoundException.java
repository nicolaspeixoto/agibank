package agibank.reportmanagement.report;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class AccumulatorNotFoundException extends RuntimeException {
    public AccumulatorNotFoundException(final String message) {
        super(message);
    }
}
