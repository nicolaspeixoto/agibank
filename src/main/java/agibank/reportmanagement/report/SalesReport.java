package agibank.reportmanagement.report;

import agibank.reportmanagement.Constants;
import agibank.reportmanagement.accumulator.ClientAccumulator;
import agibank.reportmanagement.accumulator.Accumulator;
import agibank.reportmanagement.accumulator.SaleAccumulator;
import agibank.reportmanagement.accumulator.SalesmanAccumulator;


/**
 * Created by Nicolas on 03/05/18.
 */
public class SalesReport implements Report{
    private SalesmanAccumulator salesmanAccumulator;
    private ClientAccumulator clientAccumulator;
    private SaleAccumulator saleAccumulator;

    public SalesReport(){
        this.salesmanAccumulator = new SalesmanAccumulator();
        this.clientAccumulator = new ClientAccumulator();
        this.saleAccumulator = new SaleAccumulator();
    }

    public Accumulator getAccumulator(final String type) {
        if(type.equals("001")) {
            return this.salesmanAccumulator;
        } else if (type.equals("002")) {
            return this.clientAccumulator;
        } else if (type.equals("003")) {
            return this.saleAccumulator;
        }
        throw new AccumulatorNotFoundException("Accumulator not found for type: " + type);
    }

    public void processLine(final String line){
        final String type = line.split(Constants.SEPARATOR)[0];
        final Accumulator accumulator = this.getAccumulator(type);
        accumulator.addItem(line);
    }

    public String getOutput(){
        final int clientSize = clientAccumulator.getList().size();
        final int salesmanSize = salesmanAccumulator.getList().size();
        final String expansiveSaleId = saleAccumulator.findExpensiveSale().getId();
        final String worseSalesmanName = saleAccumulator.findWorseSalesmanName();

        return "Quantidade de clientes no arquivo de entrada: " + clientSize + "\n" +
                "Quantidade de vendedor no arquivo de entrada: " + salesmanSize + "\n" +
                "ID da venda mais cara: " + expansiveSaleId + "\n" +
                "Pior vendedor: " + worseSalesmanName;
    }

}
