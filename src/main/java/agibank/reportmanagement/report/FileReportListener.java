package agibank.reportmanagement.report;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class FileReportListener {
    public static List<String> readFileLines(final File file){
        try {
            final FileInputStream inputStream = new FileInputStream(file);
            final List<String> lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
            return lines;
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static void writeOutput(final String path, final String outputFilename, final String content){
        try {
            final File file = new File(path + "/" + outputFilename);
            FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public static String getOutputFilename(final String filename){
        final String[] splitFilename = filename.split("\\.");
        return splitFilename[0] + ".done." + splitFilename[1];
    }

    public static void processFile(final File file, final String reportType, final String outputDirectory){
        final Report report = ReportFactory.getReport(reportType);
        FileReportListener.readFileLines(file).forEach(report::processLine);
        final String outputFilename = getOutputFilename(file.getName());
        final String output = report.getOutput();
        FileReportListener.writeOutput(outputDirectory, outputFilename, output);
    }

    public static void listen(final String inputDirectory, final String outputDirectory, final String targetExtension, final String reportType) {
        FileAlterationObserver observer = new FileAlterationObserver(inputDirectory);
        FileAlterationMonitor monitor = new FileAlterationMonitor(100);
        FileAlterationListener listener = new FileAlterationListenerAdaptor() {
            @Override
            public void onFileCreate(File file) {
                final String extension = FilenameUtils.getExtension(file.getName());
                if (extension.equals(targetExtension)) {
                    FileReportListener.processFile(file, reportType, outputDirectory);
                }
            }
        };
        observer.addListener(listener);
        monitor.addObserver(observer);
        try {
            monitor.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
