package agibank.reportmanagement.report;

/**
 * Created by nsanto1 on 04/05/18.
 */
public class ReportNotFoundException extends RuntimeException {
    public ReportNotFoundException(final String message) {
        super(message);
    }
}