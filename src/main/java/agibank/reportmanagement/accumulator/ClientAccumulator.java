package agibank.reportmanagement.accumulator;
import agibank.reportmanagement.domain.Client;

public class ClientAccumulator extends Accumulator<Client> {
    @Override
    public void addItem(final String item) {
        final String[] fields = this.splitFieldsFromLine(item);
        final Client client = new Client(fields[1], fields[2], fields[3]);
        this.getList().add(client);
    }
};