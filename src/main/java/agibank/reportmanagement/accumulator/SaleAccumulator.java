package agibank.reportmanagement.accumulator;
import agibank.reportmanagement.domain.Sale;
import agibank.reportmanagement.domain.SaleItem;

import java.util.*;
import java.util.stream.Collectors;

public class SaleAccumulator extends Accumulator<Sale> {

    @Override
    public void addItem(final String item) {
        final String[] fields = this.splitFieldsFromLine(item);
        final String[] saleItensString = fields[2].replace("[", "").replace("]", "").split(",");

        final List<SaleItem> saleItems = Arrays
            .stream(saleItensString)
            .map(itemString -> itemString.split("-"))
            .map(splitItem -> new SaleItem(
                    splitItem[0],
                    Integer.parseInt(splitItem[1]),
                    Double.parseDouble(splitItem[2])
                )
            )
            .collect(Collectors.toList());

        final Sale sale = new Sale(fields[1], saleItems, fields[3]);
        this.getList().add(sale);
    }

    public Sale findExpensiveSale(){
        return this.getList()
            .stream()
            .sorted(Comparator.comparingDouble(Sale::calculateTotalPrice))
            .collect(Collectors.toList())
            .get(this.getList().size() - 1);
    }

    public String findWorseSalesmanName(){
        final Map<String, Double> salesmanMap = new HashMap<>();

        this.getList().stream().forEach(sale -> {
            final String salesmanName = sale.getSalesmanName();
            final Double total = salesmanMap.get(salesmanName);
            if(total == null) {
                salesmanMap.put(salesmanName, sale.calculateTotalPrice());
            } else {
                salesmanMap.put(salesmanName, sale.calculateTotalPrice() + total);
            }
        });

        Map.Entry<String, Double> lowestAcumulatedSales = null;
        for(Map.Entry<String, Double> entry: salesmanMap.entrySet()){
            if(lowestAcumulatedSales == null){
                lowestAcumulatedSales = entry;
            } else if (lowestAcumulatedSales.getValue() > entry.getValue()) {
                lowestAcumulatedSales = entry;
            }
        }

        return lowestAcumulatedSales.getKey();
    }
};