package agibank.reportmanagement.accumulator;

import agibank.reportmanagement.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsanto1 on 04/05/18.
 */

public abstract class Accumulator<T>{
    private List<T> list;
    public Accumulator() {
        this.list = new ArrayList<>();
    }
    public List<T> getList(){
        return list;
    };

    abstract public void addItem(String item);

    public String[] splitFieldsFromLine(final String line){
        return line.split(Constants.SEPARATOR);
    }
}