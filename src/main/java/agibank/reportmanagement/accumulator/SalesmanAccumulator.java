package agibank.reportmanagement.accumulator;
import agibank.reportmanagement.domain.Salesman;

public class SalesmanAccumulator extends Accumulator<Salesman> {
    @Override
    public void addItem(final String item) {
        final String[] fields = this.splitFieldsFromLine(item);
        final Salesman salesman = new Salesman(fields[1], fields[2], Double.parseDouble(fields[3]));
        this.getList().add(salesman);
    }
};