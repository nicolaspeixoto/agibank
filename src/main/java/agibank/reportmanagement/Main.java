package agibank.reportmanagement;

import agibank.reportmanagement.report.FileReportListener;

public class Main {
    public static void main(String[] args) {
        final String homeDirectory = System.getProperty("user.home");
        final String inputDirectory = homeDirectory + "/data/in";
        final String outputDirectory = homeDirectory + "/data/out";
        FileReportListener.listen(inputDirectory, outputDirectory,"dat", "sales");
    }
}
